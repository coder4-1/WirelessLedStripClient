// including settings and libarys
#include <settings.h>
#include <WiFiClient.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Adafruit_NeoPixel.h>

// define global varibles
bool offline = true;

// declare global objects
WiFiClient wlanClient;
PubSubClient mqttClient(wlanClient);
Adafruit_NeoPixel led(LED_PIXELS, LED_PIN, NEO_GRB + NEO_KHZ800);


void setup() {
  Serial.begin(115200);
  // establish wlan connection
  if (connectWLAN()) {
    connectMQTT();
    offline = false;
  } else {
    // do something if you're offline
  }
  led.begin();
  
}

void loop() {
  if (!mqttClient.connected() && !offline) {
    reconnect();
  }
  mqttClient.loop();
  
}


bool connectWLAN() {
  Serial.println();
  Serial.print("Connecting to WLAN: ");
  Serial.println(WLAN_SSID);

  WiFi.disconnect();
  WiFi.hostname(MQTT_CLIENT_NAME);
  WiFi.begin(WLAN_SSID, WLAN_PASS);

  long startTime = millis();
  bool timedOut = false;
  while((WiFi.status() != WL_CONNECTED) && !timedOut) {
    delay(500);
    Serial.print(".");
    if ((millis()-startTime)/1000 >= WLAN_TIMEOUT) {
      timedOut = true;
    }
  }
  Serial.println();
  if (timedOut) {
    Serial.println("WLAN connect timed out!");
    WiFi.disconnect();
    return false;
  }

  Serial.println();
  Serial.print("WiFi Connected to SSID: ");
  Serial.println(WLAN_SSID);
  Serial.print("IP-Address: ");
  Serial.println(WiFi.localIP());
  Serial.print("Mac-Adress: ");
  Serial.println(WiFi.macAddress());
  Serial.println();
  Serial.println();
  return true;
}

void connectMQTT() {
  mqttClient.setServer(MQTT_BROKER, MQTT_PORT);
  mqttClient.setCallback(callback);
}

void reconnect() {
  while(!mqttClient.connected()) {
    Serial.println("Connecting to MQTT...");
    if (mqttClient.connect(MQTT_CLIENT_NAME)) {
      Serial.println("connected!");
      // subscribe to the needed topics
    } else {
      Serial.print("Error: ");
      Serial.println(mqttClient.state());
      Serial.println("failed to connect. Trying again in 5 seconds");
      delay(5000);
    }
  }
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message received [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // process MQTT message
}
